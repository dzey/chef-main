maintainer        "Dzey"
maintainer_email  "jeremie.pottier@gmail.com"
license           "Apache 2.0"
description       "Installs a Vagrant LAMP system with vhosts management and db user roles"
#long_description  IO.read(File.join(File.dirname(__FILE__), 'README.rdoc'))
version           "0.1.0"
recipe            "default", "Installs the system"
#recipe            "git::server", "Sets up a runit_service for git daemon"

%w{ ubuntu debian arch}.each do |os|
  supports os
end

%w{ runit }.each do |cb|
  depends cb
end
